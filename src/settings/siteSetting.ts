// github repo url
export const GITHUB_URL =
  (import.meta as any).env.VITE_APP_GITEE_REPOSITORY || 'https://gitee.com/mldong/mldong';

// vue-vben-admin-next-doc
export const DOC_URL = (import.meta as any).env.VITE_APP_DOC_URL || 'https://doc.mldong.com';

// site url
export const SITE_URL =
  (import.meta as any).env.VITE_APP_SITE_URL || 'https://gitee.com/mldong/mldong';
